var app = angular.module('weatherNews', ['ui.router']);
app.controller('MainCtrl', ['$scope','postFactory',function($scope,postFactory){
		postFactory.getAll();
		$scope.posts = postFactory.posts;

		$scope.addPost = function(){
		    if($scope.title === '') { return; }
		    postFactory.create({
		      	title: $scope.title,
		    });
		    $scope.title = '';
		};

	    $scope.incrementUpvotes = function(post){
	    	postFactory.upvote(post);
	    }
	}
]);

app.controller('PostCtrl', ['$scope','$stateParams','postFactory',function($scope, $stateParams, postFactory){
		$scope.post = postFactory.posts[$stateParams.id];
		postFactory.get($scope.post);
		$scope.addComment = function(){
			if($scope.body === '') { return; }
			postFactory.comment($scope.post,{
				body: $scope.body
			});
			$scope.body = '';
		};
	$scope.incrementUpvotes = function(comment){
		comment.upvotes += 1;
	};
}]);

app.factory('postFactory', ['$http',function($http){
	var o = {
		posts: []
	};
	o.getAll = function() {
    	return $http.get('/posts').success(function(data){
      		angular.copy(data, o.posts);
    	});
  	};
  	o.get = function(post) {
  		return $http.get('/posts/'+post._id).success(function(data){
  			post.comments = data.comments;
  		});
  	};
  	o.create = function(post) {
    	return $http.post('/posts', post).success(function(data){
      		o.posts.push(data);
    	});
  	};
  	o.upvote = function(post,comment) {
  		if(comment){
  			return $http.put('/posts/'+post._id+'/comment/'+comment._id+'/upvote')
  				.success(function(data){
		        	post.upvotes += 1;
		    	});
  		}else{
		    return $http.put('/posts/'+post._id+'/upvote').success(function(data){
		        post.upvotes += 1;
		    });
		}
	};
	o.comment = function(post,comment) {
		return $http.post('/posts/' + post._id + '/comments',comment).success(function(data){
			post.comments.push(data);
		});
	};
	return o;
}]);

app.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
		$stateProvider.state('home', {
			url: '/home',
			templateUrl: 'views/home.html',
			controller: 'MainCtrl'
		});
		$stateProvider.state('posts', {
			url: '/posts/{id}',
			templateUrl: 'views/posts.html',
			controller: 'PostCtrl'
		});
	$urlRouterProvider.otherwise('home');
}]);